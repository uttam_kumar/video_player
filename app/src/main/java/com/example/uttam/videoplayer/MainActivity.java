package com.example.uttam.videoplayer;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

@SuppressWarnings("ConstantConditions")
public class MainActivity extends Activity {

    private static final int PICK_VIDEO_REQUEST = 1001;
    private static final String TAG = "SurfaceSwitch";
    private MediaPlayer mMediaPlayer;
    private SurfaceHolder mFirstSurfaceHolder;
    private SurfaceHolder mSecondSurfaceHolder;
    private SurfaceHolder mActiveSurfaceHolder;
    private Uri mVideoUri= Uri.parse("http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");


    SurfaceView first;
    SurfaceView second;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);


        first = (SurfaceView) findViewById(R.id.firstSurface);
        second = (SurfaceView) findViewById(R.id.secondSurface);

        first.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                Log.d(TAG, "First surface created!");
                    mSecondSurfaceHolder =surfaceHolder;
                    switchingSurface(surfaceHolder);

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Log.d(TAG, "First surface destroyed!");
            }
        });

        second.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                Log.d(TAG, "second surface created!");
                mFirstSurfaceHolder = surfaceHolder;
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                Log.d(TAG, "second surface destroyed!");
            }
        });
    }


    private void switchingSurface(SurfaceHolder surfaceHolder) {
        mActiveSurfaceHolder = surfaceHolder;
        if (mVideoUri != null) {
            mMediaPlayer = MediaPlayer.create(getApplicationContext(), mVideoUri, mActiveSurfaceHolder);
            mMediaPlayer.start();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        //mVideoUri = null;
    }

    public void doSwitchSurface(View view) {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mActiveSurfaceHolder = mFirstSurfaceHolder == mActiveSurfaceHolder ? mSecondSurfaceHolder : mFirstSurfaceHolder;
            mMediaPlayer.setDisplay(mActiveSurfaceHolder);

        }else{
            mActiveSurfaceHolder=mFirstSurfaceHolder;
            mMediaPlayer.setDisplay(mActiveSurfaceHolder);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }


}
